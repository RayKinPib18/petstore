package com.company;

import com.company.Objects.Category;
import com.company.Objects.Pet;
import com.company.Objects.Status;
import com.company.Objects.Tag;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Requests {

    public Pet createPet() {

        int id = 121;
        Category category = new Category();
        category.setId(1);
        category.setName("Dog");
        Tag[] tags = new Tag[2];
        tags[0] = new Tag(10, "Labrador");
        tags[1] = new Tag(11, "Male");
        Pet pet = new Pet();

        pet.setId(id);
        pet.setCategory(category);
        pet.setName("Bobick");
        pet.setPhotoUrls(new String[]{"https://proprikol.ru/wp-content/uploads/2021/01/sobachki-krasivye-kartinki-1-650x406.jpg", "https://proprikol.ru/wp-content/uploads/2021/01/sobachki-krasivye-kartinki-3-650x455.jpg"});
        pet.setTags(tags);
        pet.setStatus(Status.available);

        return pet;
    }

    public void createPetPOST(Pet pet) throws IOException {
        ObjectMapper mapper = new ObjectMapper();

        CloseableHttpClient httpClientCreate = HttpClients.createDefault();
        HttpPost postCreate = new HttpPost("https://petstore.swagger.io/v2/pet");

        StringEntity stringEntity = new StringEntity(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(pet));
        postCreate.setEntity(stringEntity);
        postCreate.setHeader("Accept", "application/json");
        postCreate.setHeader("Content-type", "application/json");
        HttpResponse response = httpClientCreate.execute(postCreate);
        HttpEntity entity = response.getEntity();

        System.out.println(EntityUtils.toString(entity));
        System.out.println(response.getStatusLine());

        httpClientCreate.close();
    }
    public void updatePetPOST(Pet pet) throws IOException {

        CloseableHttpClient httpСlientUpdate = HttpClients.createDefault();
        HttpPost postChangeName = new HttpPost("https://petstore.swagger.io/v2/pet/" + pet.getId());

        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
        nvps.add(new BasicNameValuePair("name", "Sharick"));
        nvps.add(new BasicNameValuePair("status", Status.sold.name()));
        postChangeName.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));
        HttpResponse response = httpСlientUpdate.execute(postChangeName);
        HttpEntity entity = response.getEntity();

        System.out.println(EntityUtils.toString(entity));
        System.out.println(response.getStatusLine());
        httpСlientUpdate.close();

    }
    public void PetGET(Pet pet) throws IOException {
        CloseableHttpClient httpСlientGet = HttpClients.createDefault();
        HttpGet getPet = new HttpGet("https://petstore.swagger.io/v2/pet/" + pet.getId());

        CloseableHttpResponse response = httpСlientGet.execute(getPet);
        HttpEntity entity = response.getEntity();
        System.out.println(EntityUtils.toString(entity));
        httpСlientGet.close();
    }

}
